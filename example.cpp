// Copyright (C) 2014-2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of RCR.
//
// RCR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RCR is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RCR.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

#include "RCR.cxx"


int main(int argc, char *argv[])
{
  {
    fstream fs("example.cfg");
    RCR::ClassRCR rcr(fs, RCR::param::comment_beg = "#", RCR::param::regex_flags = regex::ECMAScript);
    fs.close();

    int parameter1(0);
    double parameter2(double(0));
    string parameter3("");
    vector<int> parameter4;
    vector<double> parameter5;
    vector<string> parameter6;
    vector<int> parameter7;

    try
      {
        cout << "parameter_list = " << rcr.GetParameterList() << endl;

        if (rcr.Set("parameter1", parameter1)) cout << "parameter1 = " << parameter1 << endl;
        if (rcr.Set("parameter2", parameter2)) cout << "parameter2 = " << parameter2 << endl;
        if (rcr.Set("parameter3", parameter3)) cout << "parameter3 = \"" << parameter3 << "\"" << endl;
        if (rcr.Set("parameter4", parameter4)) cout << "parameter4 = " << parameter4 << endl;
        if (rcr.Set("parameter5", parameter5)) cout << "parameter5 = " << parameter5 << endl;
        if (rcr.Set("parameter6", parameter6)) cout << "parameter6 = " << parameter6 << endl;
        if (rcr.Set("parameter7", parameter7)) cout << "parameter7 = " << parameter7 << endl;
      }
    catch (RCR::Error& err)
      {
        err.CoutWhat();
        return 1;
      }
  }


  {
    fstream fs("example.cfg");
    RCR::ClassRCR rcr(fs);
    fs.close();

    try
      {
        cout << "parameter1 = " << rcr.Get<int>("parameter1") << endl
             << "parameter2 = " << rcr.Get<double>("parameter2") << endl
             << "parameter3 = \"" << rcr.Get<string>("parameter3") << "\"" << endl;
      }
    catch (RCR::Error& err)
      {
        err.CoutWhat();
        return 1;
      }
  }


  {
    fstream fs("example.cfg");
    RCR::ClassRCR rcr(fs);
    fs.close();

    cout << "parameter8 = \"" << rcr.Get("parameter8") << "\"" << endl;

    try
      {
        RCR::ClassRCR *rcr_ptr = rcr.Sub("parameter8");

        if (rcr_ptr != NULL)
          {
            vector<string> names = rcr_ptr->GetParameterList();
            cout << "names = " << names << endl; 

            for (int i = 0; i < int(names.size()); ++i)
              cout << setw(10) << names[i] << " is " << rcr_ptr->Get<int>(names[i], "(\\d+)\\s*[fm]")
                   << " years old and is a "
                   << (rcr_ptr->Get<string>(names[i], "\\d+\\s*([fm])") == "f" ? "female" : "male") << endl;

            delete rcr_ptr;
          }
      }
    catch (RCR::Error& err)
      {
        err.CoutWhat();
        return 1;
      }
  }


  {
    fstream fs("example.cfg");
    RCR::ClassRCR rcr(fs);
    fs.close();

    try
      {
        {
          const string parameter9 = rcr.Get<string>("parameter9");
          cout << "parameter9 = \"" << parameter9 << "\"" << endl; 
        }

        {
          vector<string> parameter9;
          if (rcr.Set("parameter9", parameter9)) cout << "parameter9 = " << parameter9 << endl;
        }

        {
          vector<string> name;
          if (rcr.Set("parameter9", name, "(\\w+)\\s+\\d+\\s*[fm]")) cout << "name = " << name << endl;
          vector<int> age;
          if (rcr.Set("parameter9", age, "(\\d+)\\s*[fm]")) cout << "age = " << age << endl;
          vector<string> gender;
          if (rcr.Set("parameter9", gender, "\\s+([fm])\\s+")) cout << "gender = " << gender << endl;

          for (auto it = name.begin(); it != name.end(); it++)
            {
              const string regex = ".*" + *it + "\\s+(\\d+\\s[fm]).*";
              cout << *it << " = \"" << rcr.Get<string>("parameter9", regex) << "\"" << endl;
            }
        }
      }
    catch (RCR::Error& err)
      {
        err.CoutWhat();
        return 1;
      }
  }

  return 0;
}
