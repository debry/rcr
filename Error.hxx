// Copyright (C) 2012-2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of RCR.
//
// RCR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RCR is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RCR.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RCR_FILE_ERROR_HXX

namespace RCR
{
  /*!< Generic error.*/
  class Error : public std::exception
  {
  protected:
    string message_;

  public:
    Error(string message = "") throw();

    virtual ~Error() throw();

    virtual string What();
    void CoutWhat();
  };


  /*!< Regular expression error.*/
  class RegexError : public Error
  {
  public:
    RegexError(regex_constants::error_type code, string regular_expression) throw();

    ~RegexError() throw();

    string What();
  };


  /*!< Conversion error.*/
  class ConversionError : public Error
  {
  public:
    ConversionError(string name, string value, string type) throw();

    ~ConversionError() throw();
    
    string What();
  };
}

#define RCR_FILE_ERROR_HXX
#endif
