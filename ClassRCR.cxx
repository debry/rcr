// Copyright (C) 2014-2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of RCR.
//
// RCR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RCR is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RCR.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RCR_FILE_CLASS_RCR_CXX

#include "ClassRCR.hxx"

namespace std
{
  // Display C++ vectors.
  template<typename T>
  ostream& operator << (ostream& os, const vector<T> &v)
  {
    os << '[';
    std::copy(v.begin(), v.end() - 1, std::ostream_iterator<T>(os, ", "));
    if (! v.empty()) os << v.back();
    os << ']';

    return os;
  }


  std::ostream& operator << (std::ostream& os, const std::vector<string> &v)
  {
    os << '[';
    if (! v.empty()) os << "\"";
    std::copy(v.begin(), v.end() - 1, std::ostream_iterator<string>(os, "\", \""));
    if (! v.empty()) os << v.back() << "\"";
    os << ']';

    return os;
  }

  ostream& operator << (ostream& os, const vector<char> &v)
  {
    std::copy(v.begin(), v.end(), std::ostream_iterator<char>(os));
    return os;
  }
}


namespace RCR
{
  /*!< Convert string to anything.*/
  template<typename T>
  inline bool convert(string in, T &out)
  {
    istringstream iss(in);
    iss >> out;

    return iss.fail();
  }


  inline bool convert(string in, string &out)
  {
    out = in;
    return false;
  }


  // Remove comments.
  void ClassRCR::remove_comment(const string regular_expression)
  {
#ifdef RCR_WITH_DEBUG
    CBUG << "regular_expression = \"" << regular_expression << "\"" << endl;
#endif

    stringstream ss;

    // Config should begin with one end of line for regular expressions to work.
    ss << endl;

    try
      {
        regex expr(regular_expression, regex_flags_);
        regex_token_iterator<vector<char>::iterator> end;
        regex_token_iterator<vector<char>::iterator> it(config_.begin(), config_.end(), expr, -1);

        config_.resize(distance(it, end));

        while (it != end) ss << *it++;
      }
    catch (regex_error& e)
      {
        throw RegexError(e.code(), regular_expression);
      }

    // Config must end with one end of line for regular expressions to work.
    ss << endl;

    config_.assign(istreambuf_iterator<char>(ss), istreambuf_iterator<char>());

#ifdef RCR_WITH_DEBUG
    CBUG << "config = \"" << config_ << "\"" << endl;
#endif
  }


  // Constructors.
  ClassRCR::ClassRCR(std::iostream &config, param::base *opt1, param::base *opt2, param::base *opt3)
    : regex_flags_(RCR_REGEX_FLAGS_DEFAULT), comment_beg_(RCR_COMMENT_BEG_STR_DEFAULT),
      comment_end_(RCR_COMMENT_END_STR_DEFAULT)
  {
    // Manage optional parameters.
    param::vector opt = {opt1, opt2, opt3};

    opt.get("comment_beg", comment_beg_);
    opt.get("comment_end", comment_end_);
    opt.get("regex_flags", regex_flags_);

#ifdef RCR_WITH_DEBUG
    CBUG << "comment_beg = \"" << comment_beg_ << "\"" << endl;
    CBUG << "comment_end = \"" << comment_end_ << "\"" << endl;
    CBUG << "regex_flags = " << regex_flags_ << endl;
#endif

    config.seekg(0, std::ios::end);
    config_.reserve(config.tellg());
    config.seekg(0, std::ios::beg);

    config_.assign(istreambuf_iterator<char>(config), istreambuf_iterator<char>());

#ifdef RCR_WITH_DEBUG
    CBUG << "config = \"" << config_ << "\"" << endl;
#endif

    // Get rid of comments.
    remove_comment("(" + comment_beg_ + "[^" + comment_beg_ + comment_end_ + "]*" + comment_end_ + ")");

#ifdef RCR_WITH_DEBUG
    CBUG << "config = \"" << config_ << "\"" << endl;
#endif

    return;
  }


  // Destructor.
  ClassRCR::~ClassRCR()
  {
    return;
  }


  // Get parameter list.
  vector<string> ClassRCR::GetParameterList() const
  {
    vector<char> config;
    try
      {
        regex expr(regular_expression_list_0_, regex_flags_);
        regex_token_iterator<vector<char>::const_iterator> end;
        regex_token_iterator<vector<char>::const_iterator> it(config_.begin(), config_.end(), expr, -1);

        stringstream ss;
        while (it != end) ss << *it++;
        config.assign(istreambuf_iterator<char>(ss), istreambuf_iterator<char>());
      }
    catch (regex_error& e)
      {
        throw RegexError(e.code(), regular_expression_list_0_);
      }

    vector<string> parameter_list;
    try
      {
        regex expr(regular_expression_list_1_, regex_flags_);
        regex_token_iterator<vector<char>::const_iterator> end;
        regex_token_iterator<vector<char>::const_iterator> it(config.begin(), config.end(), expr, 1);

        if (it != end)
          {
            parameter_list.resize(distance(it, end));

            vector<string>::iterator jt = parameter_list.begin();
            while (it != end)
              *jt++ = *it++;
          }
      }
    catch (regex_error& e)
      {
        throw RegexError(e.code(), regular_expression_list_1_);
      }

    return parameter_list;
  }


  // Sub.
  ClassRCR* ClassRCR::Sub(const string key, string regular_expression) const
  {
    string value;
    if (Set(key, value, regular_expression))
      {
        stringstream config(value);
        return new ClassRCR(config);
      }
    else
      return NULL;
  }


  // Set parameters.
  template<typename T>
  bool ClassRCR::Set(const string key, T &value, string regular_expression) const
  {
    vector<string> regular_expressions;
    if (regular_expression.empty())
      regular_expressions = regular_expression_2_;
    else
      regular_expressions.push_back(regular_expression); 

    for (vector<string>::size_type i = 0; i < regular_expressions.size(); ++i)
      regular_expressions[i] = regular_expression_0_ + key + regular_expression_1_
        + regular_expressions[i] + regular_expression_3_;

#ifdef RCR_WITH_DEBUG
    CBUG << "regular_expressions = " << regular_expressions << endl;
#endif

    bool match(false);

    try
      {
        vector<string>::reverse_iterator it = regular_expressions.rbegin();
        while (! match && it != regular_expressions.rend())
          {
            regular_expression = *it++;
            regex expr(regular_expression, regex_flags_);
            match_results<vector<char>::const_iterator> results;
            if ((match = (regex_match<vector<char>::const_iterator>(config_.begin(), config_.end(), results, expr))
                 && (results.size() > 1)))
              if (convert(results.str(1), value))
                throw ConversionError(key, results.str(1), string(typeid(T).name()));
          }
      }
    catch (regex_error& e)
      {
        throw RegexError(e.code(), regular_expression);
      }

#ifdef RCR_WITH_DEBUG
    CBUG << "match ? " << (match ? "yes" : "no") << endl;
    CBUG << "value = " << value << endl;
#endif

    return match;
  }


  template<typename T>
  bool ClassRCR::Set(const string key, vector<T> &value, string regular_expression, int token) const
  {
    bool match;
    string result;

    if ((match =
         ClassRCR::Set(key, result)))
      {
        if (regular_expression.empty())
          regular_expression = "\\s*[,;\\n]?\\s*([^ ,;\\n]+)";

#ifdef RCR_WITH_DEBUG
        CBUG << "regular_expression = \"" << regular_expression << "\"" << endl;
#endif
        try
          {
            regex expr(regular_expression, regex_flags_);
            regex_token_iterator<string::iterator> end;
            regex_token_iterator<string::iterator> it(result.begin(), result.end(), expr, token);

            if ((match = (it != end)))
              {
                value.resize(distance(it, end));

                typename vector<T>::iterator jt = value.begin();
                while (it != end)
                  if (convert(*it, *jt++))
                    throw ConversionError(key, string(*it), string(typeid(T).name()));
                  else
                    it++;
              }
          }
        catch (regex_error& e)
          {
            throw RegexError(e.code(), regular_expression);
          }
      }

#ifdef RCR_WITH_DEBUG
    CBUG << "match ? " << (match ? "yes" : "no") << endl;
    CBUG << "value = " << value << endl;
#endif

    return match;
  }


  // Get parameters.
  template<typename T>
  T ClassRCR::Get(const string key, string regular_expression) const
  {
    T value;
    if (Set(key, value, regular_expression))
      return value;
    else
      return std::numeric_limits<T>::quiet_NaN();
  }


  string ClassRCR::Get(const string key, string regular_expression) const
  {
    string value;
    if (Set(key, value, regular_expression))
      return value;
    else
      return "";
  }
}
#define RCR_FILE_CLASS_RCR_CXX
#endif
