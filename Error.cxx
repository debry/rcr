// Copyright (C) 2012-2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of RCR.
//
// RCR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RCR is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RCR.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RCR_FILE_ERROR_CXX

#include "Error.hxx"

namespace RCR
{
  // Generic error.
  Error::Error(string message) throw(): message_(message)
  {
  }


  Error::~Error() throw()
  {
  }


  string Error::What()
  {
    return "Error in RCR : " + message_;
  }


  void Error::CoutWhat()
  {
    cout << this->What() << endl;
  }


  // Regular expression error.
  RegexError::RegexError(regex_constants::error_type code, string regular_expression) throw(): Error()
  {
    if (code == regex_constants::error_collate)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained an invalid collating element name.";
    else if (code == regex_constants::error_ctype)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained an invalid character class name.";
    else if (code == regex_constants::error_escape)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained an invalid escaped character, or a trailing escape.";
    else if (code == regex_constants::error_backref)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained an invalid back reference.";
    else if (code == regex_constants::error_brack)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained mismatched brackets ([ and ]).";
    else if (code == regex_constants::error_paren)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained mismatched parentheses ([ and ]).";
    else if (code == regex_constants::error_brace)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained mismatched braces ({ and }).";
    else if (code == regex_constants::error_badbrace)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained an invalid range between braces ({ and }).";
    else if (code == regex_constants::error_range)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained an invalid character range.";
    else if (code == regex_constants::error_space)
      this->message_ = "There was insufficient memory to convert the expression into a finite state machine.";
    else if (code == regex_constants::error_badrepeat)
      this->message_ = "The expression \"" + regular_expression +
        "\" contained a repeat specifier (one of *?+{) that was" +
        " not preceded by a valid regular expression.";
    else if (code == regex_constants::error_complexity)
      this->message_ = "The complexity of an attempted match against a regular expression exceeded a pre-set level.";
    else if (code == regex_constants::error_stack)
      this->message_ = "There was insufficient memory to determine whether the regular expression could match the specified character sequence.";
    else
      this->message_ =  "The expression \"" + regular_expression + "\" produced an unspecified error.";
  }


  RegexError::~RegexError() throw()
  {
  }


  string RegexError::What()
  {
    return "Regex Error in RCR : " + this->message_;
  }


  // Conversion error.
  ConversionError::ConversionError(string name, string value, string type) throw(): Error()
  {
    this->message_ = "Failed to convert parameter \"" + name +
      "\" with value \"" + value + "\" to type \"" + type + "\".";
  }


  ConversionError::~ConversionError() throw()
  {
  }


  string ConversionError::What()
  {
    return "Conversion Error in RCR : " + this->message_;
  }
}

#define RCR_FILE_ERROR_CXX
#endif
