// Copyright (C) 2014-2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of RCR.
//
// RCR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RCR is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RCR.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RCR_FILE_RCR_CXX

#include <exception>
#include <typeinfo>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <string>
#include <vector>
#include <regex>

#ifdef RCR_WITH_DEBUG
#define XSTR(x) STR(x)
#define STR(x) #x
#pragma message(STR(__cplusplus) " = " XSTR(__cplusplus))
#define CBUG cerr << __FILE__ << ":" <<__FUNCTION__ << ":" << __LINE__ << ":" 
#endif

#ifdef DISP
#undef DISP
#endif
#define DISP(x) std::cout << #x ": " << x << std::endl

#ifdef ERR
#undef ERR
#endif
#define ERR(x) std::cout << "Hermes - " #x << std::endl

namespace RCR
{
  using namespace std;
}

#define RCR_REGEX_FLAGS_DEFAULT regex::ECMAScript
#define RCR_COMMENT_BEG_STR_DEFAULT "#"
#define RCR_COMMENT_END_STR_DEFAULT "\\n"

#include "Error.cxx"
#include "ClassRCR.cxx"

namespace RCR
{
  string ClassRCR::regular_expression_0_ = "[^]*[\\n\\s]\\s*";
  string ClassRCR::regular_expression_1_ = "\\s*[\\s:=]\\s*";
  vector<string> ClassRCR::regular_expression_2_ = {"([^ :=].*\\S)", "[\\\[\\\{]([^\\\[\\\{\\\]\\\}]+)[\\\]\\\}]"};
  string ClassRCR::regular_expression_3_ = "\\s*\\n[^]*";
  string ClassRCR::regular_expression_list_0_ = "([\\\[\\\{][^\\\[\\\{\\\]\\\}]+[\\\]\\\}])";
  string ClassRCR::regular_expression_list_1_ = "\\n?\\s*([^\\s:=]+)\\s*[\\s:=].*\\n";
}

#define RCR_FILE_RCR_CXX
#endif
