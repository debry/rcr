// Copyright (C) 2014-2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of RCR.
//
// RCR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RCR is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RCR.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RCR_FILE_CLASS_RCR_HXX

namespace RCR
{
  namespace param
  {
#include "np.hxx"
    NAMED_PARAMETER(comment_beg, string, RCR_COMMENT_BEG_STR_DEFAULT);
    NAMED_PARAMETER(comment_end, string, RCR_COMMENT_END_STR_DEFAULT);
    NAMED_PARAMETER(regex_flags, regex::flag_type, RCR_REGEX_FLAGS_DEFAULT);
  }

  class ClassRCR
  {
  private:

    /*!< Configuration as a char vector.*/
    vector<char> config_;

    /*!< Regular expression flags.*/
    regex::flag_type regex_flags_;

    /*!< Comment delimiters.*/
    string comment_beg_, comment_end_;

    /*!< Regular expression pieces.*/
    static string regular_expression_0_;
    static string regular_expression_1_;
    static vector<string> regular_expression_2_;
    static string regular_expression_3_;

    /*!< Parameter list regular expression.*/
    static string regular_expression_list_0_;
    static string regular_expression_list_1_;

    /*!< Remove comments.*/
    void remove_comment(const string regular_expression);

  public:

    /*!< Constructors.*/
    ClassRCR(iostream &config, param::base *opt1 = NULL, param::base *opt2 = NULL, param::base *opt3 = NULL);


    /*!< Destructor.*/
    ~ClassRCR();


    /*!< Get parameter list.*/
    vector<string> GetParameterList() const;

    /*!< Sub.*/
    ClassRCR* Sub(const string key, string expression_regular = "") const;

    /*!< Set parameters.*/
    template<typename T>
    bool Set(const string key, T &value, string expression_regular = "") const;
    template<typename T>
    bool Set(const string key, vector<T> &value, string expression_regular = "", int token = 1) const;


    /*!< Get parameters.*/
    template<class T>
    T Get(const string key, string expression_regular = "") const;
    string Get(const string key, string expression_regular = "") const;
  };
}

#define RCR_FILE_CLASS_RCR_HXX
#endif
