
RCR, Regular expression Configuration Reader or Reading Configuration by means of Regular expressions, lets
you read configuration files from C++ using the powerful regular expressions which, unfortunately, seem
available only from GNU gcc version 4.9 and above. You will also need to pass the "-std=gnu++11" option at
compile time.