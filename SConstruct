# Copyright (C) 2014-2015 Edouard Debry
# Author(s) : Edouard Debry
#
# This file is part of RCR.
#
# RCR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RCR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RCR.  If not, see <http://www.gnu.org/licenses/>.

import os, sys, glob, distutils.sysconfig


#
# Default suer values.
#


rcr_path = os.getcwd()
cpp_flags = ["-O2", "-Wall", "-pedantic", "-std=gnu++11"]
debug = ARGUMENTS.get("debug", "no")
line = ARGUMENTS.get("line", "no")


#
# Build environment.
#


env = Environment(ENV = os.environ)
env.Replace(CONFIGURELOG = "./.scons.log")
env.Replace(CDEFINES = "_REENTRANT")
env.Replace(CPPDEFINES = "")
env.Replace(CPPFLAGS = cpp_flags)

if "g++" in env["CXX"]:
    import commands
    s, o = commands.getstatusoutput(env["CXX"] + " -dumpversion")
    if s == 0:
        version = [int(x) for x in o.split('.')]
        if not version >= [4, 9, 3]:
            print "\033[31m\033[1m# << !! \033[5mERROR\033[0m\033[31m\033[1m : " + \
                "The GNU compiler version required is at most " + str([4, 9, 3]) + \
                ", got " + str(version) + " !! >>\033[0m"
            sys.exit(1)

if debug == "yes":
    env.Replace(CCFLAGS = "-g")
    env.Append(CPPDEFINES = "RCR_WITH_DEBUG")

if line == "no":
    env.Replace(CXXCOMSTR = "[C++] $SOURCE")
    env.Replace(LINKCOMSTR = "[Linking] $TARGET")

if os.environ.has_key("LD_LIBRARY_PATH"):
    env.Append(LIBPATH = os.environ["LD_LIBRARY_PATH"].split(":"))
if os.environ.has_key("LIBRARY_PATH"):
    env.Append(LIBPATH = os.environ["LIBRARY_PATH"].split(":"))
if os.environ.has_key("CPATH"):
    env.Append(CPPPATH = os.environ["CPATH"].split(":"))
if os.environ.has_key("CPLUS_INCLUDE_PATH"):
    env.Append(CPPPATH = os.environ["CPLUS_INCLUDE_PATH"].split(":"))

for program in glob.glob("*.cpp"):
    env.Program(program[:-4], [program])
